![Build Status](https://gitlab.com/koha-community/koha-cookbook/badges/master/pipeline.svg)

---

# koha-cookbook

A collection of recipes inspired by Koha users and contributors around the world.

## GitLab CI

This project's static Pages are built by [GitLab CI][ci], following the steps
defined in [`.gitlab-ci.yml`](.gitlab-ci.yml):

## Building locally

### Prerequisits

> sudo apt-get install make python3-sphinx python3-sphinxcontrib.spelling python3-pip

> pip3 install -U sphinx-book-theme

### Workflow

To work locally with this project, you'll have to follow the steps below:

1. Fork, clone or download this project
1. Generate the documentation: `make html`

The generated HTML will be located in the location specified by `conf.py`,
in this case `_build/html`.

