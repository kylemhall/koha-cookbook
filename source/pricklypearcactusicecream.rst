Prickly Pear Cactus Ice Cream
===============================================================================

Prickly pears are easy to grow and can be found across the Western hemisphere.  They have been introduced to other locations through humans and are considered invasive in many regions (sorry Australia!).  Here in the desert of Southern New Mexico they are prolific and when the fruit (or tunas) are ripe the desert practically glows with the purple color.   While both the cactus paddles and fruit can both be eaten, this recipe calls for just the tuna.  Its flavor to me is grassy, vegetal, sweet with watermelon flavors as well.  Hard to describe, but certainly delicious!


.. image:: images/dishes/pricklypearicecream.jpg

Joy, Brendan, and Nick enjoy sharing pictures of their food, new recipes, and arguing about whether to cook pork with the fat cap up or down. They planned their recipes together and
you can find the others here:

-  Ribs and Coleslaw by Brendan

-  Dandelion Shrub by Nick

Ingredients
-------------------------------------------------------------------------------

Prickly pear puree

-  125 mL prickly pear puree (approx 6 prickly pears - red not green)

-  15 mL lime juice

-  25 grams sugar

-  1/2 of a very small ghost chile (optional -or substitute your favorite chile)

-  dash of salt

Ice cream base

-  375 mL of milk 

-  375 mL of cream

-  150 grams of sugar

-  30 grams milk powder

Preparation
-------------------------------------------------------------------------------

1.  In a pot over med to med low heat, combine milk, cream, milk powder and stir until milk powder is dissolved.
    Add the sugar and stir until dissolved.

2.  Pour the prickly pear juice into the milk mixture and stir it in. (Yes this will be bright pink).
    Turn off the heat and let steep for about 1 hour.

3.  Strain the ice cream mix and store in the refrigerator for 4 hours or more.
    When well chilled, put into your ice maker and churn accordingly to the manufacturer's instructions. 

4.  When frozen, put into the freezer or simply enjoy straight from the machine!


-------------------------------------------------------------------------------

Joy Nelson, United States

Joy is a librarian first, data nerd second.  She works for ByWater Solutions where she wrangles all things related to Koha.  She loves cooking and the color pink.
