Ribs and Coleslaw
===============================================================================

Slow cooked (on a smoker) championship baby back pork ribs.


.. image:: images/dishes/ribsandcoleslaw-final.jpg

Joy, Brendan, and Nick enjoy sharing pictures of their food, new recipes, and arguing about whether to cook pork with the fat cap up or down. They planned their recipes together and
you can find the others here:

-  Prickly pear cactus ice cream by Joy

-  Dandelion Shrub by Nick

Ingredients
-------------------------------------------------------------------------------

First is the rub: (The key here is that you want four parts - in my experiment to make this rub I ended up around 450 grams - but what you want is 1 part sugar, 1 part savory, 1 part salt, 1 part salt/savory) 
-  100 grams sugar

-  100 grams seasoning salt (I use the Lawry’s brand)

-  100 grams Montreal steak season

-  100 grams of savory

   -  25g Paprika 

   -  25g garlic powder

   -  25g cumin

   -  25g hot pepper (I use cayenne).   (This is the part that you can be creative with and create your own) 
 
Ribs

-  3 racks of babyback ribs

Coleslaw

-  1 head of cabbage 

-  3 carrots 

-  40g of mustard powder 

-  Pinch of salt

-  Pinch of pepper 

-  40 milliliters of vinegar (you can experiment with different types here - I use apple cider vinegar) 

-  Couple of splashes of water 

Preparation
-------------------------------------------------------------------------------

Coleslaw

1.  Slice the cabbage and carrots thinly 

2.  Mixed other ingredients together and toss with the carrots and cabbage

3.  Store over night in the fridge giving it a toss every so often

Ribs 

1.  Mix rub ingredients together
   .. image:: images/dishes/ribsandcoleslaw-step1.jpg

2.  Rub the ribs with what I call a single pat method.
    Pour some of the rub on and give it a single pat - both sides of the ribs.
    The rub should soak into the meat for about an hour before you put them on your grill.
   .. image:: images/dishes/ribsandcoleslaw-step1.jpg

3. Get your grill or smoker going (you want an indirect heat source).
   I usually aim for 135C.  
   Once you’ve maintained that temp - then place your ribs on the smoker.
   Let them smoke for about 3 hours or 3.5 hours and then check on them.
   At this point you want to open up your grill and do a scratch test.
   If you scratch the top of the meat and the rub is staying on the meat - you’re good to move to the next step.
   If it doesn’t pass - give it more time.  Each cut of meat is different so the timing is more an art than a science.
   .. image:: images/dishes/ribsandcoleslaw-step3.jpg

4. Now we are going to do a step called the Texas crutch.
   Take the ribs off the grill.
   .. image:: images/dishes/ribsandcoleslaw-step4a.jpg
   Get a couple of pieces of heavy aluminum foil.
   Place the ribs in the foil and using a flour sifter - sift brown sugar onto both sides of the ribs so they are covered.
   Then pour a little bit of pineapple juice about (3 us tablespoons which I think is like 45 milliliters) and about the same amount of agave syrup on the ribs.
   Wrap them up like a burrito.
   .. image:: images/dishes/ribsandcoleslaw-step4b.jpg 

5. We now place these back on the grill.
   But before we do that - the most important step is you need to give some love to the ribs.
   As you walk back to the grill give them a hug and let them know that Koha is awesome and everyone in the community is full of love.
   Smoke them for about another hour or 1.5 hours.  

6. Take them off the grill and let them rest for about 20 ~ 30 minutes.  Cut and enjoy! 

-------------------------------------------------------------------------------

Brendan A. Gallagher, United states

I like to have THE fun!  Koha has definitely been one of the most fun projects I’ve ever been involved with.  It’s about the people of the community and cooking to me has always been about sharing.
