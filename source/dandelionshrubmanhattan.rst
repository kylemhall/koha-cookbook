Dandelion shrub
===============================================================================

Dandelions, bright yellow flowers that pop up everywhere in the summer. My Dad used to make them into wine, which was always a very sweet desert wine. The taste is unique, I insists it tastes of pure yellow, but if that doesn't make any sense then think of it as liquid summer.

Sweetness is necessary to process the flower, but adding vinegar and making a shrub (drinkable vinger) cuts the sweetness and adds a complexity.

Note: If you aren't French, don't ask them what their word for dandelion means. If you are french, I promise it tastes better than it sounds


.. image:: images/dishes/dandelionshrubmanhattan.jpg

Joy, Brendan, and Nick enjoy sharing pictures of their food, new recipes, and arguing about whether to cook pork with the fat cap up or down. They planned their recipes together and
you can find the others here:

-  Ribs and coleslaw by Brendan
-  Prickly pear cactus ice cream by Joy


Ingredients
-------------------------------------------------------------------------------
Shrub

-  Dandelions
   You are going to need a large amount, I paid small children to pick a large bowl full of the flower heads
   
-  400g Sugar

-  500ml Champagne vinegar

Manhattan

-  Bourbon

-  Bitters 

Preparation
-------------------------------------------------------------------------------

1. Clean the dandelions, you want to remove the green bottoms and keep only the petals.
   You will want ~1L loosely packed

2. Mix the petals and the sugar in a non-reactive container, cover, leave in a cool place for 2-3 days
   Stirring a few times to encourage the petals to release some liquid

3. Add the vinegar and seal the mixture in a glass jar/container and leave in the refrigerator for 30 days

4. After the mixture has aged, strain through a fine strainer or cheesecloth

5. Your shrub is now ready to drink! It is a very concentrated mixture, and quite sweet, so needs diluting.

Manhattan

1. For a cocktail, the shrub can act as a sweet vermouth in a Manhattan

2. Combine 1 part shrub, 2 parts bourbon, dash of bitters

3. Shake with ice, strain into a glass, add a maraschino cherry

4. Enjoy!

Non-alcoholic
-  For a non-alcoholic drink pour 1 part shrub and 3 parts seltzer over crushed ice and stir

-------------------------------------------------------------------------------

Nick Clemens, United States

Nick lives in Vermont, likes science fiction and smiling, and works for ByWater Solutions. He is a former release manager for Koha, is part of the QA team, and enjoys working with the community.
